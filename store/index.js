import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store({
	//用来储存全局变量的方法
    state: {
		//当前播放的id
		Id:0,
		//播放id列表
		playIdList:[],
		//播放歌单信息列表
		playlist:[],
		//播放路径
		idInfo:{},
		//播放信息
		musicInfo:{},
		//当前播放的状态
		zzPlay:false,
		//播放方式
		playwaynum:1
    },
	//这个是同步方法
    mutations: {
		//播放id
		setPlayingId(state,playingId){
			state.Id = playingId
		},
		setPlayIdList(state,playIdList){
			state.playIdList = playIdList
		},
		setPlaylist(state,playlist){
			state.playlist = playlist
		},
		//播放路径
		setPlayinfo(state,idInfo){
			state.idInfo = idInfo
		},
		//播放信息
		setMusicInfo(state,musicInfo){
			state.musicInfo= musicInfo
		},
		//播放状态
		isPlay(state,isplay){
			state.zzPlay = isplay
		},
		//播放方式
		setPlaywaynum(state,num){
			state.playwaynum = num
		}
    },
	//用来处理state中的值 相当于 数组方法中的 map作用
	getters:{
		//播放指针
	    animation(state){
	        return state.zzPlay?'running':'paused'
	    },
		//是否喜欢歌曲
		islike(state){
			let likeList=uni.getStorageSync("likeMusic")
			return likeList.ids.includes(state.Id);
		}
	},
	//通过actions 可以间接调用 mutations突变方法 
	//actions是个同步方法 这里可以发送请求什么的然后通过返回的值在改变值
	actions: {
		
	}
})

export default store