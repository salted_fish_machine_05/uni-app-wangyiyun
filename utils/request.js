//同时发送异步代码的次数
let ajaxtime=0;
export function Finish(){
	return true
}
export const request=(params)=>{
    //发送一次 +1
    ajaxtime++;
    //在显示一个加载中的效果 微信的加载api功能
    //封装请求方法
    //const baseUrl="https://api.zbztb.cn/api/public/v1"
    return new Promise ((resolve,reject)=>{ /*resolve成功的回调函数  reject失败的回调函数*/
        uni.request({
            ...params,//传递过来的参数
            //url:baseUrl+params.url,
            success:(result)=>{
                resolve(result.data);
            },
            fail:(err)=>{
                reject(err)
            },
            complete:()=>{ //成功失败都执行的函数
                ajaxtime--;
                if(ajaxtime===0){
					Finish()
                }
            }
        });
          
    })
}