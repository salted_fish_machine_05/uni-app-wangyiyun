import Vue from 'vue'
import App from './App'
import weappCookie from 'weapp-cookie'

import store from './store'
Vue.prototype.$store = store

Vue.config.productionTip = false

import { request,Finish } from "./utils/request.js";
Vue.prototype.request=request
Vue.prototype.Finish=Finish

import regeneratorRuntime from "./utils/runtime.js";
Vue.prototype.regeneratorRuntime=regeneratorRuntime

import bsurl from "./utils/bsurl.js";
Vue.prototype.bsurl=bsurl

App.mpType = 'app'

const app = new Vue({
    ...App,
	request,
	regeneratorRuntime,
	bsurl,
	Finish
})
app.$mount()
