# 使用uni-app开发的仿网易云音乐2.0升级版

 **介绍** 
基于后端的仿网易云2.0，原始版请访问 https://gitee.com/salted_fish_machine_05/startLine。
该项目在原有的基础上使用uni-app升级，代码封装性，更强，更简洁，扩展性更好，且在原有的功能扩展更多模块功能，添加过度动画，优化图片加载等等。


 **软件架构** 
仿网易云音乐2.0使用的是基于vue的uni-app框架，语法使用的的javascript


 **安装教程** 
1.  下载仓库代码
2.  下载uni-app框架，可将代码运行小程序端，手机app端口。
3.  本项目采用了less对css进行预处理，运行uni时请下载less插件
4.  可使用uni-app打包成小程序或app。


 **使用说明** 

1.  app使用的api接口是基于开源的Github开发者的接口，仅供学习使用。
2.  后端接口文档地址 https://binaryify.github.io/NeteaseCloudMusicApi/#/?id=neteasecloudmusicapi
3.  后端接口地址 http://musicapi.leanapp.cn
3.  本项目后端使用的接口为开源接口 http://musicapi.leanapp.cn 如需要修改后端接口地址，
可以前往 https://binaryify.github.io/NeteaseCloudMusicApi/#/?id=neteasecloudmusicapi 文档地址下载后端文件
4.  本程序使用的全局背景音乐API只限于小程序或安卓，无法兼容H5浏览器
5.  小程序若需要在后台播放可以在后 manifest.json 源代码中（ 如微信模块配置 "mp-weixin" ）添加以下属性
	"requiredBackgroundModes": [
	    "audio",
	    "location"
	],
5.  已知问题：
（1）由于app与小程序端存在差异运行可能app本地图片无法加载，可更改组件中图片的相对路径即可。
（2）app和小程序平台不同，获取元素节点宽度存在差异，目前能在小程序稳定运行，app中播放页面可能无法拖拽音乐进度条。
 (3) 由于后台接口的问题，建议从 接口地址 http://musicapi.leanapp.cn 获取后台源码运行在自己的服务器上，直接调用开源接口
  可能会导致无法获取歌曲url和歌词，调用修改接口在 utils/bsurl.js 文件中。
（4）目前由于uni存在平台的差异，目前只对微信小程序做了兼容处理，未作条件编译。（其他的平台可能存在BUG）

 **更新说明** 
    2020年 2月15日
    第一次提交代码

    2020年 3月13日

    1新增加邮箱登录方式验证码登录，2，新增我的音乐面板，3优化各个组件加载效果

    2020年 3月16日

    1播放器优化，2修复小播放器与播页面不同步的bug，3新增加进度条拖拽，歌词联动等功能

    2020年 4月10日

    1 使用Vuex托管全局音频数据，大大优化播放器逻辑，解决了后台运行广播事件的内存泄露问题。
    2引入第三方依赖riweapp-cookie，在main.js文件引入，使小程序可以自动获取Cookies保存本地缓存，当发送请求，
    三方包能自动发送cookies，保证部分接口正常运行。（运行APP可不用引入依赖，app有自动发送cookies功能）
    3 添加播放后台播放封面，歌手与歌曲名字
    

 **参与贡献** 

1.  Ui设计风格与图标图片素材源自作者：sqaiyan 开源仓库：https://github.com/sqaiyan/NeteaseMusicWxMiniApp
2.  接口提供者 Binaryify 开源接口地址 https://binaryify.github.io/NeteaseCloudMusicApi/#/?id=neteasecloudmusicapi
3.  作者咸鱼_05号机提交代码


 **功能模块** 

1.网易手机登录

   （1）手机登录

   （2）邮箱登录

   （3）由于接口原因验证码登录关闭

2.我的音乐

   （1）创建，收藏歌单

   （2）用户设置面板

3.网易首页

   （1）推荐歌单，推荐mv,推荐新歌，推荐电台，

   （2）华语歌单列表，排行榜，

   （3）每日歌曲

   （4）小播放器联动


4.歌单与专辑播放页面

   （1）播放列表

   （2）歌单评价

5.歌手页面

   （1）热门50首

   （2）专辑

   （3）MV

   （4）歌手信息

6.播放器内核

   （1）歌曲评价

   （2）歌曲详情

   （3）歌曲是否为喜欢歌曲

   （4）歌曲播放列表

   （5）进度条拖拽

   （6）歌词联动

   （7）随机播放，列表循环，单曲播放

   （8）暂停，下一曲，上一曲

   （9）单曲，每日推荐，歌单，专辑等播放

7.歌单排行榜

   （1）热门TOP5

   （2）歌手排行榜

   （3）全球榜

8.搜索功能

   （1）单曲搜素

   （2）歌手搜素

   （3）歌单搜素

   （4）专辑搜素

   （5）用户搜素

9.查看评论

   （1）歌曲的热门，最新评论

   （2）专辑，歌单的热门，最新评论

10.个人中心

   （1）粉丝，动态，关注数目

   （2）创建与收藏歌单

   （3）个人基本信息



 **功能展示图** 

![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/100527_3651451e_5633329.jpeg "Screenshot_2020-03-03-15-03-20-406_io.dcloud.HBui.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/100539_ee8b27b6_5633329.jpeg "Screenshot_2020-03-14-12-46-40-581_com.tencent.mm.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/100551_98e3d78e_5633329.jpeg "Screenshot_2020-03-14-12-46-50-434_com.tencent.mm.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/100601_4abee885_5633329.jpeg "Screenshot_2020-03-14-13-06-25-733_com.tencent.mm.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/100612_bce68899_5633329.jpeg "Screenshot_2020-03-16-09-45-03-676_com.tencent.mm.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/100626_c5f6b06d_5633329.jpeg "Screenshot_2020-03-16-09-45-15-440_com.tencent.mm.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/100642_f7024aef_5633329.jpeg "Screenshot_2020-03-16-09-45-22-281_com.tencent.mm.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/100659_11e5fca9_5633329.jpeg "Screenshot_2020-03-16-09-45-51-522_com.tencent.mm.jpg") 

![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/100717_869931fa_5633329.jpeg "Screenshot_2020-03-16-09-46-41-677_com.tencent.mm.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/100731_f6a7eee6_5633329.jpeg "Screenshot_2020-03-16-09-46-52-994_com.tencent.mm.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/100748_54f1f091_5633329.jpeg "Screenshot_2020-03-16-09-46-59-945_com.tencent.mm.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/101123_5054ccc6_5633329.jpeg "Screenshot_2020-03-16-09-47-06-259_com.tencent.mm.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/100901_0a1d7bc1_5633329.jpeg "Screenshot_2020-03-16-09-53-57-372_io.dcloud.HBuilder.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/100911_412fd0ad_5633329.jpeg "Screenshot_2020-03-16-09-53-43-037_com.tencent.mm.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/100944_631af392_5633329.jpeg "Screenshot_2020-03-16-09-48-53-624_com.tencent.mm.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/101008_7b51e0f8_5633329.jpeg "Screenshot_2020-03-16-09-47-25-647_com.tencent.mm.jpg") 

![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/214255_d236094a_5633329.jpeg "Screenshot_2020-03-16-09-42-49-095_com.tencent.mm.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/214305_03ce002f_5633329.jpeg "Screenshot_2020-03-16-09-43-17-632_com.tencent.mm.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/214314_6f69ef87_5633329.jpeg "Screenshot_2020-03-16-09-43-32-888_com.tencent.mm.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/214328_a589b8f8_5633329.jpeg "Screenshot_2020-03-16-09-43-51-388_com.tencent.mm.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/214343_76b1b1ac_5633329.jpeg "Screenshot_2020-03-16-09-44-23-826_com.tencent.mm.jpg") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0316/214355_18d0fb27_5633329.jpeg "Screenshot_2020-03-16-09-44-54-496_com.tencent.mm.jpg") 